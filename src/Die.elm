module Die exposing (drawDie)

import Html.Events exposing (..)
import Svg exposing (..)
import Svg.Attributes exposing (..)


drawDie : Int -> msg -> List (Svg msg)
drawDie dieFace msg =
    let
        pxPerDot =
            20

        radius =
            5

        xOffset =
            650

        yOffset =
            270
    in
    [ g
        [ id "dieGroup"
        , onClick msg
        ]
        (rect
            [ x (String.fromInt (xOffset - pxPerDot))
            , y (String.fromInt (yOffset - pxPerDot))
            , width (String.fromInt (4 * pxPerDot))
            , height (String.fromInt (4 * pxPerDot))
            , rx "15"
            , ry "15"
            ]
            []
            :: List.map
                (drawDieFaceDot
                    xOffset
                    yOffset
                    pxPerDot
                    radius
                )
                (List.filter (dieDotIsOn dieFace) dieFaceDots)
        )
    ]


drawDieFaceDot : Int -> Int -> Int -> Int -> DieFaceDot -> Svg msg
drawDieFaceDot xOffset yOffset pxPerDot radius dot =
    circle
        [ cx (String.fromInt (xOffset + pxPerDot * dot.x))
        , cy (String.fromInt (yOffset + pxPerDot * dot.y))
        , r (String.fromInt radius)
        , fill "black"
        ]
        []


dieDotIsOn : Int -> DieFaceDot -> Bool
dieDotIsOn dieFace dieFaceDot =
    List.member dieFace dieFaceDot.on


type alias DieFaceDot =
    { x : Int
    , y : Int
    , on : List Int
    }


dieFaceDots =
    [ DieFaceDot 0 0 [ 2, 3, 4, 5, 6 ]
    , DieFaceDot 2 0 [ 4, 5, 6 ]
    , DieFaceDot 0 1 [ 6 ]
    , DieFaceDot 1 1 [ 1, 3, 5 ]
    , DieFaceDot 2 1 [ 6 ]
    , DieFaceDot 0 2 [ 4, 5, 6 ]
    , DieFaceDot 2 2 [ 2, 3, 4, 5, 6 ]
    ]
