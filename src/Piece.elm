module Piece exposing (..)

import Character exposing (Character)
import Figure exposing (Figure)


type alias Piece =
    { id : Int
    , figure : Figure
    , rotation : Rotation
    , revealed : Bool
    }


character : Piece -> Character
character piece =
    Figure.character piece.figure


type Rotation
    = Zero
    | Ninety
    | OneEighty
    | TwoSeventy


rotationToDegrees : Rotation -> Int
rotationToDegrees rotation =
    case rotation of
        Zero ->
            0

        Ninety ->
            90

        OneEighty ->
            180

        TwoSeventy ->
            270
