module Player exposing (..)

import Character exposing (Character(..))
import Dict.Any as AnyDict exposing (AnyDict)
import Figure exposing (Figure(..), figureToName)


type alias Player =
    { character : Character
    , captured : AnyDict String Figure Int
    , endGameMovesRemaining : Int
    }


initHuman : Player
initHuman =
    Player Human (AnyDict.empty figureToName) 5


initAnimal : Player
initAnimal =
    Player Animal (AnyDict.empty figureToName) 5


playerToName : Player -> String
playerToName player =
    case player.character of
        Human ->
            "Human"

        Animal ->
            "Animal"

        _ ->
            "Neutral"


recordCapture : Player -> Figure -> Player
recordCapture player capturedFigure =
    let
        alreadyCaptured =
            AnyDict.get capturedFigure player.captured

        newCaptured =
            case alreadyCaptured of
                Nothing ->
                    AnyDict.insert capturedFigure 1 player.captured

                Just n ->
                    AnyDict.insert capturedFigure (n + 1) player.captured
    in
    { player | captured = newCaptured }
