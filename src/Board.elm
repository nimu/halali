module Board exposing (..)

import Coords exposing (Coords, Direction(..))
import Dict exposing (Dict)
import Figure exposing (Figure(..))
import Piece exposing (Piece, Rotation(..))


type alias Board =
    Dict Coords Piece


movePiece : Board -> Piece -> Coords -> Coords -> Board
movePiece board pieceToMove origin target =
    let
        newBoard =
            Dict.insert target pieceToMove board |> Dict.remove origin
    in
    newBoard


centerPieceIndex : Int
centerPieceIndex =
    23


boardFromFigures : List Figure -> Board
boardFromFigures figures =
    List.indexedMap Tuple.pair figures
        |> List.map (\( i, figure ) -> ( indexToCoordsSkippingCenter i, figureToPiece i figure ))
        |> Dict.fromList


figuresWithMultiplicity : List Figure
figuresWithMultiplicity =
    List.repeat 2 Bear
        ++ List.repeat 6 Fox
        ++ List.repeat 2 Lumberjack
        ++ List.repeat 8 Hunter
        ++ List.repeat 8 Pheasant
        ++ List.repeat 7 Duck
        ++ List.repeat 15 Tree


indexToCoordsSkippingCenter : Int -> Coords
indexToCoordsSkippingCenter index =
    let
        correction =
            if index > centerPieceIndex then
                1

            else
                0

        x =
            modBy 7 (index + correction)

        y =
            (index + correction) // 7
    in
    ( x, y )


indexToCoords : Int -> Coords
indexToCoords index =
    let
        x =
            modBy 7 index

        y =
            index // 7
    in
    ( x, y )


figureToPiece : Int -> Figure -> Piece
figureToPiece id figure =
    Piece id figure Zero False


pieceAt : Board -> Coords -> Maybe Piece
pieceAt board coords =
    Dict.get coords board


coordsOfPiece : Board -> Piece -> Maybe Coords
coordsOfPiece board piece =
    Dict.toList board
        |> List.filter (\( k, v ) -> v == piece)
        |> List.head
        |> Maybe.map (\( k, v ) -> k)


replace : Board -> Piece -> Piece -> Board
replace board captor captee =
    let
        fromCoords =
            coordsOfPiece board captor

        toCoords =
            coordsOfPiece board captee
    in
    case ( fromCoords, toCoords ) of
        ( Just from, Just to ) ->
            Dict.remove from board |> Dict.insert to captor

        _ ->
            board


validCapture : Board -> Piece -> Piece -> Bool
validCapture board captor captee =
    let
        captorCoordinates =
            coordsOfPiece board captor

        capteeCoordinates =
            coordsOfPiece board captee
    in
    case ( captorCoordinates, capteeCoordinates ) of
        ( Just captorCoords, Just capteeCoords ) ->
            canReach captor.figure captorCoords capteeCoords
                && noObstacles board captorCoords capteeCoords
                && isCorrectlyOriented captor captorCoords capteeCoords

        _ ->
            False


validMove : Board -> Piece -> Coords -> Bool
validMove board actor targetCoords =
    let
        actorCoordinates =
            coordsOfPiece board actor
    in
    case actorCoordinates of
        Just actorCoords ->
            canReach actor.figure actorCoords targetCoords
                && noObstacles board actorCoords targetCoords

        _ ->
            False


canReach : Figure -> Coords -> Coords -> Bool
canReach figure from to =
    isRectilinear from to
        && (Figure.canTravel figure <| Coords.distance from to)


noObstacles : Board -> Coords -> Coords -> Bool
noObstacles board from to =
    let
        xFrom =
            Tuple.first from

        yFrom =
            Tuple.second from

        xTo =
            Tuple.first to

        yTo =
            Tuple.second to

        intermediateCoords =
            if xFrom == xTo then
                List.range (min yFrom yTo) (max yFrom yTo) |> List.map (\r -> ( xFrom, r ))

            else if yFrom == yTo then
                List.range (min xFrom xTo) (max xFrom xTo) |> List.map (\r -> ( r, yFrom ))

            else
                []

        isEmptyOrEndpoint : Coords -> Bool
        isEmptyOrEndpoint coords =
            case pieceAt board coords of
                Nothing ->
                    True

                Just _ ->
                    List.member coords [ from, to ]
    in
    List.all isEmptyOrEndpoint intermediateCoords


isCorrectlyOriented : Piece -> Coords -> Coords -> Bool
isCorrectlyOriented captor captorCoords capteeCoords =
    case captor.figure of
        Hunter ->
            case ( captor.rotation, Coords.direction captorCoords capteeCoords ) of
                ( Zero, Right ) ->
                    True

                ( Ninety, Down ) ->
                    True

                ( OneEighty, Left ) ->
                    True

                ( TwoSeventy, Up ) ->
                    True

                _ ->
                    False

        _ ->
            True


isRectilinear : Coords -> Coords -> Bool
isRectilinear from to =
    (Tuple.first from == Tuple.first to)
        || (Tuple.second from == Tuple.second to)
