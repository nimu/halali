module Coords exposing (..)


type alias Coords =
    ( Int, Int )


type Direction
    = Up
    | Right
    | Down
    | Left



-- Manhattan distance between two coordinates


distance : Coords -> Coords -> Int
distance from to =
    let
        deltaX =
            Tuple.first from - Tuple.first to |> abs

        deltaY =
            Tuple.second from - Tuple.second to |> abs
    in
    deltaX + deltaY


direction : Coords -> Coords -> Direction
direction from to =
    let
        deltaX =
            Tuple.first from - Tuple.first to

        deltaY =
            Tuple.second from - Tuple.second to
    in
    if deltaY == 0 then
        if deltaX > 0 then
            Left

        else
            Right

    else if deltaY > 0 then
        Up

    else
        Down
