module Character exposing (Character(..), characterToColor, characterToDarkColor, toString)

import Palette


type Character
    = Human
    | Animal
    | Poultry
    | Arbor


toString : Character -> String
toString character =
    case character of
        Human ->
            "human"

        Animal ->
            "animal"

        Poultry ->
            "poultry"

        Arbor ->
            "arbor"


characterToColor : Character -> String
characterToColor character =
    case character of
        Human ->
            Palette.brown

        Animal ->
            Palette.blue

        _ ->
            Palette.green


characterToDarkColor : Character -> String
characterToDarkColor character =
    case character of
        Human ->
            Palette.darkBrown

        Animal ->
            Palette.darkBlue

        _ ->
            Palette.darkGreen
