module Main exposing
    ( Game
    , Model
    , Msg(..)
    , drawBoard
    , init
    , initialGame
    , main
    , subscriptions
    , update
    , view
    )

import Board exposing (Board, boardFromFigures, indexToCoords, indexToCoordsSkippingCenter, pieceAt, validCapture, validMove)
import Browser
import Character exposing (Character(..))
import Coords exposing (Coords)
import Dict
import Dict.Any as AnyDict
import Figure exposing (Figure, canCapture, character, figureToName)
import Html exposing (..)
import Html.Attributes exposing (href, src)
import List
import Palette
import Piece exposing (Piece, Rotation(..), rotationToDegrees)
import Player exposing (Player, initAnimal, initHuman)
import Point exposing (Point)
import Random exposing (Generator, generate)
import Random.Extra exposing (sample)
import Random.List exposing (shuffle)
import Slot exposing (Slot(..))
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Svg.Events exposing (onClick)


main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


setHistory : List Game -> Model -> Model
setHistory history model =
    { model | history = history }


type alias Game =
    { human : Player
    , animal : Player
    , turn : Character
    , board : Board
    }


setTurn : Character -> Game -> Game
setTurn turn game =
    { game | turn = turn }


asTurnIn : Game -> Character -> Game
asTurnIn game turn =
    setTurn turn game


switchTurn : Game -> Game
switchTurn game =
    if game.turn == Animal then
        { game | turn = Human }

    else
        { game | turn = Animal }


type alias Model =
    { game : Game
    , selectedPiece : Maybe Piece
    , history : List Game
    , errorMsg : String
    }


setGame : Game -> Model -> Model
setGame game model =
    { model | game = game }


asGameIn : Model -> Game -> Model
asGameIn model game =
    setGame game model


clearSelection : Model -> Model
clearSelection model =
    { model | selectedPiece = Nothing }


setBoard : Board -> Game -> Game
setBoard board game =
    { game | board = board }


asBoardIn : Game -> Board -> Game
asBoardIn game board =
    setBoard board game


resetErrorMsg : Model -> Model
resetErrorMsg model =
    { model | errorMsg = "" }


initialGame : Game
initialGame =
    Game initHuman initAnimal Animal Dict.empty


init : () -> ( Model, Cmd Msg )
init _ =
    ( Model initialGame Nothing [] "", generate (FiguresShuffled 1) (shuffle Board.figuresWithMultiplicity) )



-- UPDATE


type Msg
    = Flip Coords
    | ClickedOn Coords
    | FiguresShuffled Int (List Figure)
    | RotateFigure Int Rotation


rotationGenerator : Generator Rotation
rotationGenerator =
    sample [ Zero, Ninety, OneEighty, TwoSeventy ]
        |> Random.map (Maybe.withDefault Zero)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Flip flippedCoords ->
            let
                flipMapper coords piece =
                    if flippedCoords == coords then
                        { piece | revealed = True }

                    else
                        piece

                oldGame =
                    model.game

                newModel =
                    Dict.map flipMapper model.game.board
                        |> asBoardIn oldGame
                        |> switchTurn
                        |> asGameIn model
            in
            ( clearSelection newModel, Cmd.none )

        FiguresShuffled iteration shuffledFigures ->
            if iteration > 0 then
                ( model, generate (FiguresShuffled <| iteration - 1) (shuffle shuffledFigures) )

            else
                let
                    oldGame =
                        model.game

                    newModel =
                        boardFromFigures shuffledFigures
                            |> asBoardIn oldGame
                            |> asGameIn model
                in
                ( newModel, generate (RotateFigure 48) rotationGenerator )

        RotateFigure figureIndex rotation ->
            if figureIndex > -1 then
                let
                    oldBoard =
                        model.game.board

                    entries =
                        Dict.toList oldBoard

                    mapper index ( k, v ) =
                        if index == figureIndex then
                            ( k, { v | rotation = rotation } )

                        else
                            ( k, v )

                    newBoard =
                        Dict.fromList <| List.indexedMap mapper entries
                in
                ( newBoard
                    |> asBoardIn model.game
                    |> asGameIn model
                , generate (RotateFigure (figureIndex - 1)) rotationGenerator
                )

            else
                ( model, Cmd.none )

        ClickedOn clickedCoords ->
            let
                pieceClicked =
                    pieceAt model.game.board clickedCoords

                selectedPiece =
                    model.selectedPiece

                -- nothing selected, nothing clicked    -> nothing
                -- nothing selected, something clicked  -> select
                -- something selected, nothing clicked  ->  try to move
                -- something selected, something clicked  ->
                -- -- if same character  -> select
                -- if diff character  -> attempt to capture
            in
            case ( selectedPiece, pieceClicked ) of
                ( Nothing, Nothing ) ->
                    ( model, Cmd.none )

                ( Nothing, Just c ) ->
                    ( selectIfInTurn model c, Cmd.none )

                ( Just s, Nothing ) ->
                    let
                        selectedCoords =
                            Board.coordsOfPiece model.game.board s
                    in
                    case selectedCoords of
                        Just selCoords ->
                            ( attemptToMove model s selCoords clickedCoords, Cmd.none )

                        Nothing ->
                            ( model, Cmd.none )

                ( Just s, Just c ) ->
                    if canCapture s.figure c.figure then
                        ( attemptToCapture model s c, Cmd.none )

                    else
                        ( selectIfInTurn model c, Cmd.none )


selectIfInTurn : Model -> Piece -> Model
selectIfInTurn model piece =
    if
        character piece.figure
            == model.game.turn
            || character piece.figure
            == Poultry
    then
        { model | selectedPiece = Just piece }

    else
        clearSelection model


attemptToMove : Model -> Piece -> Coords -> Coords -> Model
attemptToMove model piece originCoords targetCoords =
    if validMove model.game.board piece targetCoords then
        let
            newBoard =
                Board.movePiece model.game.board piece originCoords targetCoords
        in
        newBoard
            |> asBoardIn model.game
            |> switchTurn
            |> asGameIn model
            |> clearSelection

    else
        model


attemptToCapture : Model -> Piece -> Piece -> Model
attemptToCapture model captor captee =
    if
        Figure.canCapture captor.figure captee.figure
            && validCapture model.game.board captor captee
    then
        Board.replace model.game.board captor captee
            |> asBoardIn model.game
            |> recordCapture captee.figure
            |> switchTurn
            |> asGameIn model
            |> clearSelection

    else
        selectIfInTurn model captee


recordCapture : Figure -> Game -> Game
recordCapture capturedFigure game =
    case game.turn of
        Human ->
            { game | human = Player.recordCapture game.human capturedFigure }

        _ ->
            { game | animal = Player.recordCapture game.animal capturedFigure }



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


mainSvgAttributes : List (Svg.Attribute Msg)
mainSvgAttributes =
    [ viewBox "0 0 800 800"
    , fill "white"
    , stroke "black"
    , strokeWidth "3"
    , Html.Attributes.style "border" "solid black 1px"
    ]


view : Model -> Html Msg
view model =
    div [ width "100vw" ]
        (List.append
            (viewPlayersStats model)
            [ viewGameUnderway model ]
        )


viewGameUnderway : Model -> Html Msg
viewGameUnderway model =
    svg mainSvgAttributes
        (List.concat
            [ -- drawUndoButton (List.length model.history)
              drawBackground
            , drawBoard model.game.board model.selectedPiece
            , drawErrorMessage model.errorMsg
            ]
        )


drawBackground : List (Svg Msg)
drawBackground =
    [ rect
        [ x "0"
        , y "0"
        , width <| String.fromInt (config.pxPerPositionPoint * 7 + config.positionOffset)
        , height <| String.fromInt (config.pxPerPositionPoint * 7 + config.positionOffset)
        , fill Palette.darkGreen
        ]
        []
    , rect
        -- top
        [ x <| String.fromInt (config.pxPerPositionPoint * 3)
        , y "0"
        , width <| String.fromInt (config.pxPerPositionPoint + config.positionOffset)
        , height <| String.fromInt (config.pxPerPositionPoint // 2)
        , fill Palette.yellow
        ]
        []
    , rect
        -- bottom
        [ x <| String.fromInt (config.pxPerPositionPoint * 3)
        , y <| String.fromFloat (config.pxPerPositionPoint * 6.5 + config.positionOffset)
        , width <| String.fromInt (config.pxPerPositionPoint + config.positionOffset)
        , height <| String.fromInt (config.pxPerPositionPoint // 2)
        , fill Palette.yellow
        ]
        []
    , rect
        -- left
        [ x "0"
        , y <| String.fromInt (config.pxPerPositionPoint * 3)
        , width <| String.fromInt (config.pxPerPositionPoint // 2)
        , height <| String.fromInt (config.pxPerPositionPoint + config.positionOffset)
        , fill Palette.yellow
        ]
        []
    , rect
        -- right
        [ x <| String.fromFloat (config.pxPerPositionPoint * 6.5 + config.positionOffset)
        , y <| String.fromInt (config.pxPerPositionPoint * 3)
        , width <| String.fromInt (config.pxPerPositionPoint // 2)
        , height <| String.fromInt (config.pxPerPositionPoint + config.positionOffset)
        , fill Palette.yellow
        ]
        []
    ]


viewPlayersStats : Model -> List (Html Msg)
viewPlayersStats model =
    List.map (drawPlayerStats model) [ model.game.human, model.game.animal ]


drawPlayerStats : Model -> Player -> Html Msg
drawPlayerStats model player =
    let
        imgHref =
            "img/" ++ Character.toString player.character ++ "_player.png"

        capturedPieces =
            if player.character == Human then
                model.game.human.captured

            else
                model.game.animal.captured

        opacity =
            if model.game.turn == player.character then
                "1"

            else
                "0.4"
    in
    div
        [ Html.Attributes.style "background-color" <| Character.characterToColor player.character
        , Html.Attributes.style "opacity" opacity
        , class "player-stats"
        ]
        [ img
            [ src imgHref
            , class "player-avatar"
            ]
            []
        , div [ class "captures-container" ]
            (List.indexedMap drawCaptures (AnyDict.toList capturedPieces))
        ]


drawCaptures : Int -> ( Figure, Int ) -> Html Msg
drawCaptures idx ( figure, nr ) =
    let
        imgHref =
            "img/" ++ figureToName figure ++ ".png"
    in
    div [ class "captured" ]
        [ img
            [ src imgHref
            ]
            []
        , span
            [ Html.Attributes.style "font-size" "20pt"
            ]
            [ Html.text <| String.fromInt nr ]
        ]


drawBoard : Board -> Maybe Piece -> List (Svg Msg)
drawBoard board selection =
    List.range 0 48
        |> List.map (\i -> ( indexToCoords i, indexToCoords i |> pieceAt board ))
        |> List.map (drawSlot selection)


drawSlot : Maybe Piece -> ( Coords, Maybe Piece ) -> Svg Msg
drawSlot selection ( coords, maybeOccupant ) =
    case maybeOccupant of
        Just occupant ->
            drawPieceAtPosition selection ( coords, occupant )

        Nothing ->
            drawEmptySlot coords


drawPieceAtPosition : Maybe Piece -> ( Coords, Piece ) -> Svg Msg
drawPieceAtPosition selection ( coords, piece ) =
    if piece.revealed then
        drawRevealedPieceAtPosition selection ( coords, piece )

    else
        drawUnrevealedPieceAtPosition ( coords, piece )


drawUnrevealedPieceAtPosition : ( Coords, Piece ) -> Svg Msg
drawUnrevealedPieceAtPosition ( coords, piece ) =
    let
        xValue =
            String.fromInt (config.positionOffset + Tuple.first coords * config.pxPerPositionPoint)

        yValue =
            String.fromInt (config.positionOffset + Tuple.second coords * config.pxPerPositionPoint)
    in
    rect
        [ x xValue
        , y yValue
        , height <| String.fromFloat config.positionRadius
        , width <| String.fromFloat config.positionRadius
        , fill "darkgreen"
        , onClick (Flip coords)
        ]
        []


drawEmptySlot : Coords -> Svg Msg
drawEmptySlot coords =
    let
        xValue =
            String.fromInt (config.positionOffset + Tuple.first coords * config.pxPerPositionPoint)

        yValue =
            String.fromInt (config.positionOffset + Tuple.second coords * config.pxPerPositionPoint)
    in
    rect
        [ x xValue
        , y yValue
        , height <| String.fromFloat config.positionRadius
        , width <| String.fromFloat config.positionRadius
        , fill "gray"
        , onClick (ClickedOn coords)
        ]
        []


drawRevealedPieceAtPosition : Maybe Piece -> ( Coords, Piece ) -> Svg Msg
drawRevealedPieceAtPosition selection ( coords, piece ) =
    let
        imgHref : String
        imgHref =
            "img/" ++ figureToName piece.figure ++ ".png"

        xValue =
            String.fromInt (config.positionOffset + Tuple.first coords * config.pxPerPositionPoint)

        yValue =
            String.fromInt (config.positionOffset + Tuple.second coords * config.pxPerPositionPoint)

        cx =
            String.fromInt (config.positionOffset + Tuple.first coords * config.pxPerPositionPoint + config.positionRadius // 2)

        cy =
            String.fromInt (config.positionOffset + Tuple.second coords * config.pxPerPositionPoint + config.positionRadius // 2)

        degrees =
            String.fromInt <| rotationToDegrees piece.rotation

        rectangle =
            case selection of
                Just sel ->
                    if sel == piece then
                        [ rect
                            [ x xValue
                            , y yValue
                            , height <| String.fromFloat config.positionRadius
                            , width <| String.fromFloat config.positionRadius
                            , stroke <| Figure.figureToColor piece.figure
                            , strokeWidth "7"
                            , fill "none"
                            ]
                            []
                        ]

                    else
                        []

                Nothing ->
                    []
    in
    g []
        (List.append
            [ image
                [ xlinkHref imgHref
                , x xValue
                , y yValue
                , height <| String.fromFloat config.positionRadius
                , width <| String.fromFloat config.positionRadius
                , transform <|
                    "rotate("
                        ++ degrees
                        ++ ", "
                        ++ cx
                        ++ ","
                        ++ cy
                        ++ ")"
                , onClick (ClickedOn coords)
                ]
                []
            ]
            rectangle
        )



-- global config start


config =
    { pxPerPositionPoint = 110
    , positionRadius = 100
    , positionOffset = 10
    , statsWidth = 200
    , statsHeight = 760
    }


drawUndoButton : Int -> List (Svg Msg)
drawUndoButton historySize =
    let
        xOffset =
            670

        yOffset =
            40
    in
    [ g
        [ id "undoGroup"

        -- , onClick PopHistory
        ]
        [ circle
            [ cx (String.fromInt xOffset)
            , cy (String.fromInt yOffset)
            , r "30"
            , fill "lightgrey"
            ]
            []
        , Svg.text_
            [ x (String.fromInt (xOffset + 60))
            , y (String.fromInt (yOffset + 5))
            , strokeWidth "1"
            ]
            [ Svg.text (String.fromInt historySize)
            ]
        , Svg.path
            [ d <|
                String.join " "
                    [ "M "
                    , String.fromInt (xOffset + 10)
                    , String.fromInt (yOffset + 0)
                    , " h -20"
                    , " l 10 -5"
                    , " m -10 5"
                    , " l 10 5"
                    ]
            , stroke "black"
            , fill "none"
            ]
            []
        ]
    ]


drawErrorMessage : String -> List (Svg Msg)
drawErrorMessage errorMsg =
    [ Svg.text_
        [ x (String.fromFloat (config.pxPerPositionPoint * 7))
        , y (String.fromFloat (config.pxPerPositionPoint * 11.75))
        , fontSize "32"
        , fill "black"
        , textAnchor "middle"
        ]
        [ Svg.text errorMsg ]
    ]
