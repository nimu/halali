module Palette exposing (..)


green =
    "#198c09"


darkGreen =
    "#115509"


blue =
    "#3c3ac1"


darkBlue =
    "#252473"


brown =
    "#6d4816"


darkBrown =
    "#4b2d14"


yellow =
    "#F4D03F"
