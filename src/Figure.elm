module Figure exposing (..)

import Character exposing (Character(..))
import Palette


type Figure
    = Bear
    | Fox
    | Lumberjack
    | Hunter
    | Pheasant
    | Duck
    | Tree


character : Figure -> Character
character figure =
    if figure == Bear || figure == Fox then
        Animal

    else if figure == Lumberjack || figure == Hunter then
        Human

    else if figure == Duck || figure == Pheasant then
        Poultry

    else
        Arbor


value : Figure -> Int
value figure =
    case figure of
        Bear ->
            10

        Fox ->
            5

        Lumberjack ->
            5

        Hunter ->
            5

        Pheasant ->
            3

        Duck ->
            2

        Tree ->
            2


canCapture : Figure -> Figure -> Bool
canCapture captor captee =
    List.member captee (captures captor)


captures : Figure -> List Figure
captures captor =
    case
        captor
    of
        Bear ->
            [ Lumberjack, Hunter ]

        Fox ->
            [ Pheasant, Duck ]

        Lumberjack ->
            [ Tree ]

        Hunter ->
            [ Bear, Fox, Pheasant, Duck ]

        Pheasant ->
            []

        Duck ->
            []

        Tree ->
            []


type Range
    = Range Int


range : Figure -> Range
range figure =
    case figure of
        Bear ->
            Range 1

        Lumberjack ->
            Range 1

        Tree ->
            Range 0

        _ ->
            Range 100


canTravel : Figure -> Int -> Bool
canTravel figure distance =
    let
        figureRange =
            range figure
    in
    case figureRange of
        Range r ->
            r >= distance


figureToName : Figure -> String
figureToName figure =
    case figure of
        Bear ->
            "bear"

        Lumberjack ->
            "lumberjack"

        Tree ->
            "tree"

        Fox ->
            "fox"

        Hunter ->
            "hunter"

        Pheasant ->
            "pheasant"

        Duck ->
            "duck"


figureToColor : Figure -> String
figureToColor figure =
    Character.characterToColor <| character figure
