module Slot exposing (..)

import Piece exposing (Piece)
import Point exposing (Point)


type Slot
    = EmptySlot Point
    | OccupiedSlot Point Piece


slotToPoint : Slot -> Point
slotToPoint slot =
    case slot of
        EmptySlot p ->
            p

        OccupiedSlot p _ ->
            p
